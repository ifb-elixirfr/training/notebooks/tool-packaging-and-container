# Tool packaging and container

This JupyterNotebook aims to introduce the concept of tool packaging and container

⏱️ Duration: 3h

📑 Outlines:

- Conda
  - Installation
  - Looking for a package
  - Installing an environment
  - Activating environments
  - Playing with Conda
- Apptainer
  - Basic usage
  - Playing with Apptainer
    - Build your own image
    - Use your image
  - Sharing images
